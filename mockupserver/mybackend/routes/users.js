const express = require('express')
const router = express.Router()
const usersContrller = require('../controller/UsersController')

/* GET users listing. */
router.get('/', usersContrller.getUsers)

router.get('/:id', usersContrller.getUser)

router.post('/', usersContrller.addUser)

router.put('/', usersContrller.updateUser)

router.delete('/:id', usersContrller.deleteUser)

module.exports = router