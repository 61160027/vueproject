const User = require('../models/User')

const userContrller = {
  async addUser(req, res, next) {
    const payload = req.body
    console.log(payload)
    const user = new User(payload)
    try {
      await user.save()
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateUser(req, res, next) {
    const { id } = req.params
    const payload = req.body
    try {
      const user = await User.updateOne({ _id: payload._id }, payload)
      res.json(user)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async deleteUser(req, res, next) {
    const { id } = req.params
    try {
      const user = await User.deleteOne({ _id: id })
      res.json(user)
    } catch (error) {
      res.status(500).send(error)
    }

  },
  getUsers(req, res, next) {
    User.find({}, { name: 1,email: 1,password:1, gender: 1 })
      .then(users => {
        res.json(users)
      })
      .catch(err => {
        res.status(500).send(err)
      })
  },
  async getUser(req, res, next) {
    const { id } = req.params
    try {
      const user = await User.findById(id)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = userContrller